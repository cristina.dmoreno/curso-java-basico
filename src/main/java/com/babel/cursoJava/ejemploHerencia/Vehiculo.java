package com.babel.cursoJava.ejemploHerencia;

public abstract class Vehiculo {
    // Orientación en grados, de 0 a 360
    private int direccion = 0;
    // Velocidad en km/h
    protected int velocidad = 0;


    public abstract void acelerar();
    public abstract void frenar();
    public void girar(int grados){
        // Modulo 360, por ejemplo 350º + 20º, equivale a 10º
        direccion = (direccion + grados) % 360;
    }

    @Override
    public String toString(){
        return this.getClass() +  ": dirección: " + direccion + ", velocidad: " + velocidad;
    }

}
