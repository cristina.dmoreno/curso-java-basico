package com.babel.cursoJava.ejemploHerencia;

public class Coche extends Vehiculo{
    @Override
    public void acelerar() {
        velocidad += 10;
    }

    @Override
    public void frenar() {
        velocidad -= 15;
    }
}
