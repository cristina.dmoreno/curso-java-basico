package com.babel.cursoJava.ejemploHerencia;

public class Bicicleta extends Vehiculo{
    @Override
    public void acelerar() {
        velocidad += 2;
    }

    @Override
    public void frenar() {
        velocidad -= 5;
    }
}
