package com.babel.cursoJava.ejemploHerencia;

public class Kart extends Coche{
    @Override
    public void girar(int gradosGiro){
        // Simula un sobreviraje, ante un giro brusco el jodío derrapa y jira demasiado.
        if(Math.abs(gradosGiro) > 30){
            gradosGiro *= 2;
        }

        // Invoco el método original (grados es privado y no lo puedo modificar)
        super.girar(gradosGiro);
    }
}
