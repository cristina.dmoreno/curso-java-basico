package com.babel.cursoJava.ejemploReferenciasObjetos;

public class PruebaObjetos {

    public PruebaObjetos(){
    }

    public void probarReferenciasObjetos(){
        Persona sabio = new Persona("Sabio");
        Persona grunyon= new Persona("Gruñón");
        Persona feliz = new Persona("Feliz");
        Persona dormilon = new Persona("Dormilón");
        Persona timido  = new Persona("Timido");
        Persona mocoso = new Persona("Mocoso");
        Persona mudito = new Persona("Mudito");

        System.out.println("Los 7 enanitos:");
        System.out.println(getDatosPersona(sabio));
        System.out.println(getDatosPersona(grunyon));
        System.out.println(getDatosPersona(feliz));
        System.out.println(getDatosPersona(dormilon));
        System.out.println(getDatosPersona(timido));
        System.out.println(getDatosPersona(mocoso));
        System.out.println(getDatosPersona(mudito));

        System.out.println();
        System.out.println();
        System.out.println("Ahora tenemos una persona copiota");
        Persona copiota = null; // aqui no ha reservado memoria ni tiene contenido
        copiota = mudito;
        System.out.println("COPIOTA de mudito --> " + getDatosPersona(copiota));
        copiota = mocoso;
        System.out.println("COPIOTA de mocoso --> " + getDatosPersona(copiota));


        System.out.println();
        System.out.println();
        // Vamos a cambiar los nombres de los enanitos
        System.out.println("Cambiamos nombre en método por valor");
        cambiaNombre(mocoso.getNombre());
        System.out.println(getDatosPersona(copiota));
        System.out.println("COPIOTA de mocoso " + getDatosPersona(copiota));

        System.out.println();
        System.out.println("Cambiamos nombre en método por referencia");
        cambiaNombre(mocoso);
        System.out.println(getDatosPersona(copiota));
        System.out.println("COPIOTA de mocoso " + getDatosPersona(copiota));

        // Lo mejor, si es posible, que hay veces que no, es hacer lo que tengamos que hacer y devolver el objeto cambiado
        // así somos conscientes que dentro de nuestro método se han modificado datos
        System.out.println();
        System.out.println("Cambiamos nombre en método por valor y devolvemos la cadena del cambio");
        mocoso.setNombre(devuelveNombreCambiadoPorValor(mocoso.getNombre()));
        System.out.println(getDatosPersona(copiota));
        System.out.println("COPIOTA de mocoso " + getDatosPersona(copiota));


    }

    private void cambiaNombre(String nombre){
        nombre = nombre + " Nadie";
    }

    private String devuelveNombreCambiadoPorValor(String nombre){
        return nombre + " Nadie";
    }

    private void cambiaNombre(Persona persona){
        persona.setNombre(persona.getNombre() + " Nadie");
    }


    private String getDatosPersona(Persona persona){
        String datos = "Objeto: " + persona.toString() + " - Contenido: " + persona.getNombre();
        return datos;
    }

}
