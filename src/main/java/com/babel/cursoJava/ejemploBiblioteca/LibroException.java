package com.babel.cursoJava.ejemploBiblioteca;

public class LibroException extends Exception{

    LibroException(String mensaje){
        super(mensaje);
    }
}
