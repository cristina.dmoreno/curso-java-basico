package com.babel.cursoJava.ejemploBiblioteca;

import java.util.ArrayList;
import java.util.List;

public class Libro {
    private String titulo;
    private Autor autor;
    private String isbn;
    private Genero genero;
    int ultimoCapitulo = -1;
    private List<String> capitulos;
    private int edadMinima;

    Libro() {}

    Libro(String titulo, String isbn, Genero genero){
        this.titulo = titulo;
        this.isbn = isbn;
        this.genero = genero;
        this.capitulos = new ArrayList<>();
    }

    Libro(String titulo, String nombreAutor, String isbn, Genero genero){
        this.titulo = titulo;
        this.autor = new Autor(nombreAutor);
        this.isbn = isbn;
        this.genero = genero;
        this.capitulos = new ArrayList<>();
    }

    @Override
    public boolean equals(Object otro){
        if (otro == this)
            return true;
        if (!(otro instanceof Libro))
            return false;
        Libro otroLibro = (Libro) otro;
        return this.getTitulo().equals(otroLibro.getTitulo());
    }

    @Override
    public final int hashCode() {
        int hashcode = 12;
        return hashcode * this.getTitulo().hashCode();
    }

    @Override
    public String toString(){
        return "[" + genero + "] "+ this.titulo + ". Autor: " + autor.getNombre() + ". Edad minima: " + edadMinima;
    }

    public void nuevoCapitulo(String nuevoCapitulo){
        this.capitulos.add(nuevoCapitulo);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public List<String> getCapitulos() {
        return this.capitulos;
    }

    public void setCapitulos(List<String> capitulos) {
        this.capitulos = capitulos;
    }

    public int getEdadMinima() {
        return edadMinima;
    }

    public void setEdadMinima(int edadMinima) {
        this.edadMinima = edadMinima;
    }
}
