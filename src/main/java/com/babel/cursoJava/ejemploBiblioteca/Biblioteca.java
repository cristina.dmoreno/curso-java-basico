package com.babel.cursoJava.ejemploBiblioteca;

import java.util.ArrayList;
import java.util.List;

public class Biblioteca {

    private List<Libro> libros;

    public Biblioteca(){
        this.libros = new ArrayList<>();
        this.cargaLibros();
    }

    public List<Libro> getLibros(){
        return this.libros;
    }

    public void listarLibrosBiblioteca() throws LibroException {
        for(Libro libro: this.libros){
            if (libro == null) {
                // mi excepcion nueva
                throw new LibroException("libro no existe");
            }else{
                System.out.println(libro.toString());
            }
        }
    }

    public void listarLibrosByAutor(String autor){
        System.out.println("Libros del autor: " + autor);
        for(Libro libro: this.libros){
            if (libro != null && libro.getAutor().getNombre().equals(autor)){
                System.out.println(libro.toString());
            }
        }
    }

    public void listarLibrosByGenero(Genero genero){
        System.out.println("Libros del genero: " + genero);
        for(Libro libro: this.libros){
            if (libro != null && libro.getGenero() == genero){
                System.out.println(libro.toString());
            }
        }
    }

    public void mostrarInformacionLibro(Libro libro){
        System.out.println(libro.toString());
        System.out.println("Capitulos:");
        for(int i = 0; i < libro.getCapitulos().size(); i++){
            System.out.println("Capitulo " + (i+1) + " - " + libro.getCapitulos().get(i));
        }
    }

    public void actualizarEdadMinima(){
        for (Libro libro: this.libros){
            if (libro != null) {
                switch (libro.getGenero()) {
                    case INFANTIL:
                        libro.setEdadMinima(2);
                        break;
                    case NOVELA:
                        libro.setEdadMinima(15);
                        break;
                    default:
                        libro.setEdadMinima(99);
                }
            }
        }
    }

    private void cargaLibros(){
        // creamos los libros
        Libro libro1 = new Libro("La historia interminable", "Michael Ende", "ISBN-1", Genero.NOVELA);
        Libro libro2 = new Libro("El juego del angel", "Carlos Ruiz Zafon", "ISBN-2", Genero.NOVELA);
        Libro libro3 = new Libro("El monstruo de colores", "Anna Lenas", "ISBN-3", Genero.INFANTIL);
        Libro libro4 = new Libro("Fuera de aqui horrible monstruo verde", "Ed Emberley", "ISBN-4", Genero.INFANTIL);
        Libro libro5 = new Libro("Rimas y leyendas", "Gustavo Adolfo Becquer", "ISBN-5", Genero.POESIA);

        // asignamos los libros (sus referencias de memoria) a una posición del array
        this.libros.add(libro1);
        this.libros.add(libro2);
        this.libros.add(libro3);
        this.libros.add(libro4);
        this.libros.add(libro5);

        Libro libro6 = null;
        this.libros.add(libro6);


        // creamos capitulos en algunos de los libros
        nuevoCapituloLibro(libro1);
        nuevoCapituloLibro(this.libros.get(2));
    }

    void nuevoCapituloLibro(Libro libro){
        libro.nuevoCapitulo("Primer capitulo");
        libro.nuevoCapitulo("Segundo capitulo");
        libro.nuevoCapitulo("Tercer capitulo");
    }

}
