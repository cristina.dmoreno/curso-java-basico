package com.babel.cursoJava.ejemploBiblioteca;

public class Autor {
    private String nombre;

    Autor(String nombre){
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
