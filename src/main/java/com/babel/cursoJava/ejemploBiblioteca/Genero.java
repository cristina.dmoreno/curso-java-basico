package com.babel.cursoJava.ejemploBiblioteca;

public enum Genero {
    NOVELA, RELATO, ENSAYO, POESIA, INFANTIL
}
