package com.babel.cursoJava;

import com.babel.cursoJava.ejemploBiblioteca.Biblioteca;
import com.babel.cursoJava.ejemploBiblioteca.Genero;
import com.babel.cursoJava.ejemploBiblioteca.Libro;
import com.babel.cursoJava.ejemploBiblioteca.LibroException;
import com.babel.cursoJava.ejemploHerencia.Bicicleta;
import com.babel.cursoJava.ejemploHerencia.Coche;
import com.babel.cursoJava.ejemploHerencia.Kart;
import com.babel.cursoJava.ejemploHerencia.Vehiculo;
import com.babel.cursoJava.ejemploHerenciaNuevo.Animal;
import com.babel.cursoJava.ejemploHerenciaNuevo.Caballo;
import com.babel.cursoJava.ejemploHerenciaNuevo.Mamifero;
import com.babel.cursoJava.ejemploHerenciaNuevo.Perro;
import com.babel.cursoJava.ejemploReferenciasObjetos.PruebaObjetos;

import java.text.SimpleDateFormat;
import java.util.*;

public class ClasePrincipal {


    public static void main(String... args) {
        //pruebaVehiculos();
        //pruebaBiblioteca();
        //pruebaReferenciasObjetos();
        //pruebaHerenciaAnimales();

        int numero = 0;
        try {
            numero = Integer.parseInt("asdf");
        }catch (NumberFormatException miExcepcion){
            System.out.println("no es un numero " + miExcepcion);
            numero = -1;
        }

        System.out.println(numero);

    }

    public static void pruebaHerenciaAnimales(){
        Animal animal = new Animal("Gustavo", "insectos", 3);
        Mamifero mamifero = new Mamifero("Moby Dick", "plancton", 5, 12);
        Perro perro = new Perro("Tobby", "carne", 7, 2, "caniche");
        Perro perro2 = new Perro("Ringo", "carne", 8, 3, "labrador");
        Perro perro3 = new Perro("Lassie", "carne", 10, 5, "border collie");
        Caballo caballo = new Caballo("Rocinante", "hierba", 8, 12, "español", 2);

        List<Animal> animales = new ArrayList<>();
        animales.add(animal);
        animales.add(mamifero);
        animales.add(perro);
        animales.add(perro2);
        animales.add(perro3);
        animales.add(caballo);
//        System.out.println("Lista de los animales");
//        for (Animal miAnimal : animales) {
//            System.out.println(miAnimal);
//        }
//
//        System.out.println("Accion comer");
//        for (Animal miAnimal : animales) {
//            miAnimal.comer();
//        }


        Map<String, List<Animal>> animalesPorTipoComida = new HashMap<>();
        for (Animal miAnimal : animales) {
            if (!animalesPorTipoComida.containsKey(miAnimal.getTipoAlimentacion())){
                animalesPorTipoComida.put(miAnimal.getTipoAlimentacion(), new ArrayList<>());
            }
            animalesPorTipoComida.get(miAnimal.getTipoAlimentacion()).add(miAnimal);
        }

        System.out.println("Animales por tipo de comida");
        System.out.println(animalesPorTipoComida);

        /*
        {
            hierba=[Caballo -> Mamifero -> Animal -> Rocinante, hierba, 8, 12, español, 2],
            insectos=[Animal -> Gustavo, insectos, 3],
            carne=[Perro -> Mamifero -> Animal -> Tobby, carne, 7, 2, caniche,
                   Perro -> Mamifero -> Animal -> Ringo, carne, 8, 3, labrador,
                   Perro -> Mamifero -> Animal -> Lassie, carne, 10, 5, border collie],
            plancton=[Mamifero -> Animal -> Moby Dick, plancton, 5, 12]}
        */

    }

    public static void pruebaReferenciasObjetos(){
        PruebaObjetos pruebaObjetos = new PruebaObjetos();
        pruebaObjetos.probarReferenciasObjetos();
    }

    public static void pruebaBiblioteca(){
        Biblioteca biblioteca = new Biblioteca();
        System.out.println("Listar libros biblioteca");
//        List<Libro> libros = biblioteca.getLibros();
//
//        Map<Genero, List<Libro>> mapaPorGenero = new HashMap<>();
//        for (Libro libro: libros) {
//            if(!mapaPorGenero.containsKey(libro.getGenero())){
//                mapaPorGenero.put(libro.getGenero(), new ArrayList<>());
//            }
//            mapaPorGenero.get(libro.getGenero()).add(libro);
//        }
//
//        System.out.println(mapaPorGenero);


        try{
            biblioteca.listarLibrosBiblioteca();
        }catch(LibroException e){
            System.out.println("Excepcion localizada: " + e.getMessage());
        }finally{
            System.out.println("Ejecución finally");
        }

//
//        biblioteca.listarLibrosByAutor("Michael Ende");
//
//        biblioteca.listarLibrosByGenero(Genero.INFANTIL);
//
//        System.out.println("Actualizar edad minima");
//        biblioteca.actualizarEdadMinima();
//        biblioteca.listarLibrosBiblioteca();


    }

    public static void pruebaVehiculos(){
        // Polimorfismo
        Vehiculo[] vehiculos = {
                new Coche(),
                new Bicicleta(),
                new Kart()
        };

        // Abstracción
        for (int i = 0; i < 3; i++) {
            for (Vehiculo vehiculo : vehiculos) {
                vehiculo.acelerar();
            }
        }
        vehiculos[0].frenar();
        for (Vehiculo vehiculo : vehiculos) {
            vehiculo.girar(50);
        }
        for (Vehiculo vehiculo : vehiculos) {
            System.out.println(vehiculo.toString());
        }


        int numero = (int)(Math.random()*100);
        int numeroDeVecesQueEjecuta = 0;
        while(numero < 20){
            numero = (int)(Math.random()*100);
            ++numeroDeVecesQueEjecuta;
        }
    }


}
