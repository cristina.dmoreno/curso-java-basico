package com.babel.cursoJava.ejemploHerenciaNuevo;

public class Caballo extends Mamifero{

    private String comida;
    private int ejercicio;

    public Caballo(String nombre, String tipoAlimentacion, int edad, int gestacion, String comida, int ejercicio) {
        super(nombre, tipoAlimentacion, edad, gestacion);
        this.comida = comida;
        this.ejercicio = ejercicio;
    }

    public Caballo(){
        super();
    }

    public String toString() {
        return "Caballo -> " + super.toString() + ", " + this.comida + ", " + this.ejercicio;
    }

    @Override
    public void comer(){
        System.out.println("Come el caballo");
    }

    public String getComida() {
        return comida;
    }

    public void setComida(String comida) {
        this.comida = comida;
    }

    public int getEjercicio() {
        return ejercicio;
    }

    public void setEjercicio(int ejercicio) {
        this.ejercicio = ejercicio;
    }
}
