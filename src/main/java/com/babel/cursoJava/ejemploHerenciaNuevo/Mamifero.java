package com.babel.cursoJava.ejemploHerenciaNuevo;

public class Mamifero extends Animal{

    private int gestacion;

    public Mamifero(String nombre, String tipoAlimentacion, int edad, int gestacion){
        super(nombre, tipoAlimentacion, edad);
        this.gestacion = gestacion;
    }

    public Mamifero(){
        super();
    }

    @Override
    public void comer(){
        System.out.println("Come el mamifero");
    }

    public String toString() {
        return "Mamifero -> " + super.toString() + ", " + this.gestacion;
    }

    public int getGestacion() {
        return gestacion;
    }

    public void setGestacion(int gestacion) {
        this.gestacion = gestacion;
    }
}
