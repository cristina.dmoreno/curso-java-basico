package com.babel.cursoJava.ejemploHerenciaNuevo;

public class Animal implements Accion{
    private String nombre;
    private String tipoAlimentacion;
    private int edad;

    public Animal(String nombre, String tipoAlimentacion, int edad){
        this.nombre = nombre;
        this.tipoAlimentacion = tipoAlimentacion;
        this.edad = edad;
    }

    public Animal(){
    }

    public void comer(){
        System.out.println("Come el animal");
    }

    public String toString(){
        return "Animal -> " + this.nombre + ", " + this.tipoAlimentacion + ", " + this.edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoAlimentacion() {
        return tipoAlimentacion;
    }

    public void setTipoAlimentacion(String tipoAlimentacion) {
        this.tipoAlimentacion = tipoAlimentacion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}
