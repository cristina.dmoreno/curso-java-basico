package com.babel.cursoJava.ejemploHerenciaNuevo;

public class Perro extends Mamifero{

    private String raza;

    public Perro(String nombre, String tipAlimentacion, int edad, int gestacion, String raza) {
        super(nombre, tipAlimentacion, edad, gestacion);
        this.raza = raza;
    }

    public Perro(){}

    public void ladrar(){
        System.out.println("El perro ladra");
    }

    public String toString() {
        return "Perro: " + super.toString() + ", " + this.raza;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }
}
